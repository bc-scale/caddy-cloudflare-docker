# From: https://caddy.community/t/how-to-guide-caddy-v2-cloudflare-dns-01-via-docker/8007

FROM caddy:builder AS builder

RUN caddy-builder \
    github.com/caddy-dns/cloudflare

FROM caddy:latest

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
